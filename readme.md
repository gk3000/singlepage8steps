# A simple responsive website with Grid

## by Barcelona Code School

This is a 8-step workshop to build a simple responsive single page website with CSS Grid.

You can clone this repo with ```git clone https://gitlab.com/barcelonacodeschool/css-grid-basics``` and follow the build process step by step. 

In every step new stuff being added and marked with comments in the html or css file. 

You can check results with these links:

[Step 1](http://simple-responsive-website-with-grid.surge.sh/01)

[Step 2](http://simple-responsive-website-with-grid.surge.sh/02)

[Step 3](http://simple-responsive-website-with-grid.surge.sh/03)

[Step 4](http://simple-responsive-website-with-grid.surge.sh/04)

[Step 5](http://simple-responsive-website-with-grid.surge.sh/05)

[Step 6](http://simple-responsive-website-with-grid.surge.sh/06)

[Step 7](http://simple-responsive-website-with-grid.surge.sh/07)

[Step 8](http://simple-responsive-website-with-grid.surge.sh/08)